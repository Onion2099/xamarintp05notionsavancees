﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tp05.Domains.Models.Services;
using Tp05.Domains.Models.DbModels;
using System.Collections.ObjectModel;

namespace Tp05.Services
{
    public class TwitterService : ITwitterService
    {

        public List<Tweet> Tweets
        {
            // FakeData
            get
            {
                User user = new User() { Login = "jean", Password = "bonneau" };

                return new List<Tweet>()
                {
                    new Tweet(){User = user, Data ="Aujourd'hui j'ai posté une photo de mon chat. ", CreatedAt = DateTime.Now},
                    new Tweet(){User = user, Data ="Je suis un tweet complètement random", CreatedAt = DateTime.Now},
                    new Tweet(){User = user, Data ="tweet banned", CreatedAt = DateTime.Now},
                    new Tweet(){User = user, Data ="......fhdshfsdhfpsd", CreatedAt = DateTime.Now}
                };
            }
        }


        public Boolean Authenticate(User user)
        {
            return Tweets.Select(x => x.User).Any(x => x.Login == user.Login && x.Password == user.Password);
        }

    }
}
