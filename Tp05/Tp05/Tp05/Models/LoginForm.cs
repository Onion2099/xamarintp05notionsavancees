﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using Tp05.Domains.Models.DbModels;
using Tp05.Domains.Models.Services;
using Tp05.Services;
using Xamarin.Essentials;
using Xamarin.Forms;



namespace Tp05.Models
{
    public class LoginForm
    {
        private readonly ITwitterService twitterService;
        private readonly Entry login;
        private readonly Entry password;
        private readonly Xamarin.Forms.Switch isRemind;
        // TODO > Balai sur code mort
        /*private readonly VisibilitySwitch visibilitySwitch;*/
        private readonly ErrorForm error;
        private INavigation navigation;

        private User user;

        public LoginForm(Entry login, Entry password, Xamarin.Forms.Switch isRemind, View loginForm, View tweetForm, Label errorLabel, Button button, INavigation navigation)
        {
            this.twitterService = new TwitterService();
            this.navigation = navigation;
            this.login = login;
            this.password = password;
            this.isRemind = isRemind;
            // TODO > Balai sur code mort
            /*this.visibilitySwitch = new VisibilitySwitch(loginForm, tweetForm);*/
            this.error = new ErrorForm(errorLabel);
            button.Clicked += Button_ClickedAsync;
        }

        private void Button_ClickedAsync(object sender, EventArgs e)
        {
            Debug.WriteLine("User > ClicOn Btn loginForm");

            // Xamarin.Essentials > Si le device de l'Utilisateur à une connexion internet effective
            // alors les vérifs du form peuvent avoir lieu, sinon erreur.
            if (Connectivity.NetworkAccess == NetworkAccess.Internet)
            {
                if (this.IsValid())
                {
                    if (twitterService.Authenticate(this.user))
                    {
                        this.error.Hide();

                        Debug.WriteLine("User Login OKAY > Navigation tweetPage");
                        this.navigation.PushAsync(new TweetPage());

                        // TODO > Balai sur code mort
                        /*this.visibilitySwitch.Switch();*/
                    }
                    else
                    {
                        // Attente données valides (FakeData > TwitterService)
                        this.error.Error = "Memo > Login=jean mdp=bonneau";
                        this.error.Display();
                    }
                }
                else
                {
                    this.error.Display();
                }
            }
            else
            {
                this.error.Error = "Skynet à finit par plomber ta connexion ? 3244";
                this.error.Display();
            }
        }

        // verif des données saisies du formulaire 
        public Boolean IsValid()
        {
            Boolean result = true;

            User user = new User();
            user.Login = login.Text;
            user.Password = password.Text;
            Boolean isRemind = this.isRemind.IsToggled;

            bool haveError = false;
            StringBuilder stringBuilder = new StringBuilder();

            if (String.IsNullOrEmpty(user.Login) || user.Login.Length < 3)
            {
                haveError = true;
                stringBuilder.Append("L'identifiant ne peut pas être null et doit posséder au moins 3 caractères.");
            }

            if (String.IsNullOrEmpty(user.Password) || user.Password.Length < 6)
            {
                if (haveError)
                {
                    stringBuilder.Append("\n");
                }
                haveError = true;
                stringBuilder.Append("Le mot de passe ne peut pas être null et doit posséder au moins 6 caractères.");
            }

            if (haveError)
            {
                this.error.Error = stringBuilder.ToString();
            }

            result = !haveError;
            this.user = user;

            return result;
        }

        
    }
}
