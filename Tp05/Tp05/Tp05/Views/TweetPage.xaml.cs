﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tp05.Domains.Models.Services;
using Tp05.Models;
using Tp05.Services;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Tp05
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TweetPage : ContentPage
    {
        private TwitterService tweetService;
        public TweetPage()
        {
            InitializeComponent();
            tweetService = new TwitterService();
            this.ListeTweets.ItemsSource = tweetService.Tweets;

        }
    }
}