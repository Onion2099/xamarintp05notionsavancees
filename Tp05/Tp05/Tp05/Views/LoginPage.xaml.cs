﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tp05.Domains.Models.DbModels;
using Tp05.Domains.Models.Services;
using Tp05.Models;
using Tp05.Services;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Tp05
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LoginPage : ContentPage
    {
        private View tweetForm;

        public LoginPage()
        {
            InitializeComponent();
            
            new LoginForm(this.login, this.password, this.isRemind, this.loginForm, this.tweetForm, this.errorLabel, this.connection, this.Navigation);
        }

    }
}