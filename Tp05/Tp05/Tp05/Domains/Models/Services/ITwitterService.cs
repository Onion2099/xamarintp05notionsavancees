﻿using System;
using System.Collections.Generic;
using System.Text;
using Tp05.Domains.Models.DbModels;

namespace Tp05.Domains.Models.Services
{
    public interface ITwitterService
    {
        Boolean Authenticate(User user);
        List<Tweet> Tweets { get; }
    }

}
